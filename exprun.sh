#!/bin/sh
MAINDIR=$(pwd)
MIRRORPATH="${MAINDIR}/rubygem-simp-metadata/scratch/mirror"
GEMDIR="/scratch/gems"
git clone https://github.com/heliocentric/rubygem-simp-metadata
cd rubygem-simp-metadata
git checkout SIMP-3078
mkdir -p scratch/data
mkdir -p scratch/mirror
bundle install --path=${GEMDIR} >/dev/null 2>/dev/null

bundle exec ruby -I lib ./main.rb mirror >/dev/null 2>/dev/null

for pupmod in $(ls ${MIRRORPATH} | grep ^simp-)
do
	cd "${MIRRORPATH}/${pupmod}"
	echo "${pupmod} - beginning"
	PUPPET_VERSION=5.0.0 bundle install --path=${GEMDIR} >/dev/null 2>/dev/null
	bundle exec rake syntax >/dev/null 2>/dev/null
	bundle exec rake lint >/dev/null 2>/dev/null
	bundle exec rake spec >/dev/null 2>/dev/null
	PUPPET_VERSION=5.0.0 bundle exec rake acceptance >/dev/null 2>/dev/null
	ERRNO=$?
    if [ "${ERRNO}" != "0" ] ; then
        echo "${pupmod} - failed"
    else
        echo "${pupmod} - passed"
    fi
done
